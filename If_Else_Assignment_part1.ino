/*

  Project:      IF Assignment : Photo Sensor and Button Press Reactive LEDs
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   September 24, 2019
  Based on:     Arduino Lesson 6: Digital Inputs; Simon Monk; https://learn.adafruit.com/adafruit-arduino-lesson-6-digital-inputs/
                DGIF 2002: Assignment 1 - Blink and Fade; Nicole Vella; https://gitlab.com/satoshiswife/dgif2002---01---blink/blob/master/Arduino_Lesson_1-Blink-Fade.ino 
*/


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600); // turn on console log

  pinMode(9, OUTPUT); // blue LED
  pinMode(10, OUTPUT); // red LED
  pinMode(11, OUTPUT); // green LED

  pinMode(13, INPUT); // button input

}

void loop() {
  // put your main code here, to run repeatedly:

 Serial.println(analogRead(0)); // the photo sensor is connected to the analog 0 input on the arduino
                                // output to console the brightness level being detected by the photosensor

  if (analogRead(0) >= 650  ) { // if brightness is greater than or equal to 650; turn on the blue LED at low power; turn off all other LEDs
    analogWrite(9, 1);
    analogWrite(10, 0);
    analogWrite(11, 0);
  } else if (analogRead(0) > 399 && analogRead(0) < 650) { // if brightness is between 400 and 649; turn on the red LED at medium power; turn off all other LEDs
    analogWrite(9, 0);
    analogWrite(10, 127);
    analogWrite(11, 0);
  } else if (analogRead(0) < 400) {  // if brightness is below 400; turn on the green LED at full power; turn off all other LEDs
    analogWrite(9, 0);
    analogWrite(10, 0);
    analogWrite(11, 255);
  }


  if (digitalRead(13) == HIGH) {     // a button is connected to digital pin 13 on the arduino; when pressed, run the following code

    for (int i = 0; i <= 255; i++) { // run this statement 255 times
      analogWrite(10, i);            // set the brightness of the red led to the value of i (fade up)
      delay(1);                      // delay the iterative statement by 1 ms
    }

    for (int i = 255; i >= 0; i--) { // run this statement 255 times
      analogWrite(10, i);            // set the brightness of the red led to the value of i (fade down)
      delay(1);
    }

    for (int i = 0; i <= 255; i++) { // run this statement 255 times
      analogWrite(9, i);             // set the brightness of the blue led to the value of i (fade up)
      delay(1);                      // delay the iterative statement by 1 ms
    }

    for (int i = 255; i >= 0; i--) { // run this statement 255 times
      analogWrite(9, i);             // set the brightness of the blue led to the value of i (fade down)
      delay(1);                      // delay the iterative statement by 1 ms
    }

    for (int i = 0; i <= 255; i++) { // run this statement 255 times
      analogWrite(11, i);            // set the brightness of the green led to the value of i (fade up)
      delay(1);                      // delay the iterative statement by 1 ms
    }

    for (int i = 255; i >= 0; i--) { // run this statement 255 times
      analogWrite(11, i);            // set the brightness of the green led to the value of i (fade down)
      delay(1);                      // delay the iterative statement by 1 ms
    }
    
  } else if (digitalRead(13) == LOW) {
    
    return;                          // return nothing if the button isn't pressed
    
  }

}